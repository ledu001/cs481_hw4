﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CS481_HW4
{
    public partial class ItemDetails : ContentPage
    {
        public ItemDetails(string Text, string source)
        {
            InitializeComponent();
            ShowName.Text = Text;

            ImageCall.Source = new UriImageSource()
            {
                Uri = new Uri(source)
            };
            if (ShowName.Text == "Secret Miyu")
            {
                Secret.TextColor = Color.Purple;
            }
        }
    }
}
