﻿public class Item
{
    public string Text { get; set; }
    public string Image { get; set; }

    public override string ToString()
    {
        return Text;
    }
}