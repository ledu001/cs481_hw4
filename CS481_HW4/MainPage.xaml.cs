﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace CS481_HW4
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        ObservableCollection<Item> listItems = new ObservableCollection<Item>();
        ViewCell lastCell;

        protected void ListItems_Refreshing(object sender, EventArgs e)
        {
            listItems.Clear();
            listItems.Add(new Item { Text = "Siamese", Image = "https://www.thesprucepets.com/thmb/DdBGw730sIZwIdKmufcrN92f7i8=/960x0/filters:no_upscale():max_bytes(150000):strip_icc()/35493166_2113126082300521_5592447779063463936_n-5b69b61946e0fb002562c234.jpg" });
            listItems.Add(new Item { Text = "Secret Miyu", Image = "https://i.ytimg.com/vi/J---aiyznGQ/hqdefault.jpg" });

            listCat.EndRefresh();
        }

        public MainPage()
        {
            InitializeComponent();
            listItems.Add(new Item { Text = "Persian", Image = "https://www.catster.com/wp-content/uploads/2018/11/persian-cat-face.jpg" });
            listItems.Add(new Item { Text = "Bengal", Image = "https://assets3.thrillist.com/v1/image/2622128/size/tmg-facebook_social.jpg" });
            listItems.Add(new Item { Text = "Sphinx", Image = "https://upload.wikimedia.org/wikipedia/commons/e/e8/Sphinx2_July_2006.jpg" });
            listItems.Add(new Item { Text = "Miyu", Image = "https://i.ytimg.com/vi/J---aiyznGQ/hqdefault.jpg" });
            listCat.ItemsSource = listItems;
        }

        public async void OnMore(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var detail = (Item)mi.CommandParameter;
            await Navigation.PushAsync(new ItemDetails(detail.Text, detail.Image));
        }

        public void OnDelete(object sender, EventArgs e)
        {
            var mi = ((MenuItem)sender);
            var Deleteitem = (Item)mi.CommandParameter;
            listItems.Remove(Deleteitem);
        }

    }
}
